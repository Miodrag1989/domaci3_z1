﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci3_z1
{
    class Program
    {
        static void Main(string[] args)
        {
            Skup s = new Skup(5);
            for(int i = 0; i < s.BrojElemenataNiza; i++)
            {
                Console.WriteLine($"Unesite {i + 1}. element niza:");
                int.TryParse(Console.ReadLine(), out int broj);
                s.Ucitavanje(broj, i);
            }
            s.Provera(45);
            s.Prikaz();
        }
    }
}
