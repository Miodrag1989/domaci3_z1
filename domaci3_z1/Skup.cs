﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci3_z1
{
    class Skup
    {
        private int _brojElemenata;
        private int[] _nizElemenata;


        public Skup()
        {
            _brojElemenata = 0;
        }

        public Skup(int f)
        {
            this._brojElemenata = f;
            this._nizElemenata = new int[this._brojElemenata];
        }


        public int BrojElemenataNiza
        {
            get
            {
                return this._brojElemenata;
            }
        }

        public void Provera(int x)
        {
            if(this._nizElemenata.Contains(x))
            {
                Console.WriteLine($"Broj {x} pripada skupu.");
            }
            else
            {
                Console.WriteLine($"Broj {x} ne pripada skupu.");
            }
                
            
        }

        public void Ucitavanje(int x, int i)
        {
            if (i < this._nizElemenata.Length)
            {
                this._nizElemenata[i] = x;
                
            }
            else
            {
                Console.WriteLine("Nije moguce uneti novi element!");
            }
        }

        public void Prikaz()
        {
            for(int i = 0; i < this._nizElemenata.Length; i++)
            {
                Console.Write("{0} ", this._nizElemenata[i]);
            }
        }



    }
}
